package proyecto_EMPRESA_AMAZING;
public abstract class Transporte {
	
	private String tipoVehiculo;
	private String patente;
	private Integer volumenCarga;
	private double valorViaje;
	
	
	public Transporte(String patente, Integer volumenCarga, double valorViaje,String tipoVehiculo) {
		this.patente = patente;
		this.volumenCarga = volumenCarga;
		this.valorViaje = valorViaje;
		this.tipoVehiculo=tipoVehiculo;
	
	}
	public String getTipoVehiculo() {
		return tipoVehiculo;
	}

	public String getPatente() {
		return patente;
	}

	public Integer getVolumenCarga() {
		return volumenCarga;
	}

	public double getValorViaje() {
		return valorViaje;
	}
	public abstract boolean sonIdenticos (Object o);
	
	public abstract boolean cargar (Paquete p);
	
	public abstract String listarCarga() ;
	
	public abstract double calcularCosto();
	
	public abstract int cantPaquetesCargados();
	
	
}

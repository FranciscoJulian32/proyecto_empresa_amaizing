package proyecto_EMPRESA_AMAZING;

import java.util.HashMap;
import java.util.Map;

public class TransporteUtilitarios extends Transporte{
	private Integer costoPorCargaExtra;
	private Map<Integer,Paquete> paquetes; 
	private Integer maxPaq;
	private Integer cantidadCargados;
	
	public TransporteUtilitarios(String patente, Integer volumenCarga, double valorViaje, Integer costoPorCargaExtra) {
		super(patente, 2000 , valorViaje,"Utilitarios");
		this.paquetes=new HashMap<>();
		this.costoPorCargaExtra=costoPorCargaExtra;
		this.maxPaq=60;
		this.cantidadCargados=0;
	}
//INFORMA COSTO POR VIAJE
	public double calcularCosto() {
		if ( cantPaquetesCargados()>3)
			return super.getValorViaje()+costoPorCargaExtra;
		return super.getValorViaje();
		
	}
//INFORMAR PATENTE
	public String informarPatente() {
		return super.getPatente();
	}
//INFORME EL VOLUMEN TOTAL DE LA CARGA
	public Integer getVolumenCarga() {
		int cont =0;
		for (Paquete p: paquetes.values()) {
			cont =+ p.informarVolumen();
		}
		return cont;
	}
//CARGA EL VEHICULO
@Override
	public boolean cargar(Paquete paq) {
		if (cantidadCargados <maxPaq) {
			paquetes.put(paq.informarCodigoPaquete(),paq);
			cantidadCargados++;

			return true;
		}
		return false;
	}
	public int cantPaquetesCargados() {
		return paquetes.size();
	}
//INFORMA EL ESPACIO DISPO DEL VEHICULO 
	private boolean consultarEspacio() {
		return cantPaquetesCargados() <= maxPaq; 
	} 
	private Map<Integer,Paquete> informarCarga(){
		return paquetes;
	}
//ESTA CARGADO 
		public boolean estaCargado(){
			return cantPaquetesCargados()>0;
		}
//SON IDENTICOS
		@Override 
		public boolean sonIdenticos(Object o) {
			if (!(o instanceof TransporteUtilitarios)||o==null) {
				return false;
			}
			TransporteUtilitarios utilitario = (TransporteUtilitarios) o;
			if (utilitario.informarPatente().equals(super.getPatente())) {
				return false;
			}
			if (utilitario.cantidadCargados!= this.cantidadCargados) {
				return false;
			}
			if (utilitario.cantPaquetesCargados()==0||this.cantPaquetesCargados()==0) {
				return false;
			}
			return mismaCarga(utilitario)&&tieneTodos(utilitario);
		}
//AUX
		private  boolean mismaCarga(TransporteUtilitarios utilitario ) {
			if (utilitario.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
				return false;
			}
			boolean ret = false;
			for (Paquete p:paquetes.values()) {
				Paquete p1 = p;
				for(Paquete paq:utilitario.informarCarga().values()) {
					if (p1.informarCodigoPaquete()!=paq.informarCodigoPaquete()) {
						ret = ret || p1.sonPaquetesIguales(paq);
					}
				}
			}
			return ret;
		}
//AUX
		public boolean tieneTodos(TransporteUtilitarios utilitario ) {
			if (utilitario.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
				return false;
			}

			for (Paquete p:paquetes.values()) {
				if (!esta(p,informarCarga()))
						return false;	
				}
			return true;
		}
		public boolean esta(Paquete p,Map<Integer, Paquete> map) {
			if (!map.containsValue(p))
				return false;
			return true;
		}
		
//LISTA LA CARGA
@Override 
	public String listarCarga() {
		StringBuilder info = new StringBuilder();
		for (Paquete p: paquetes.values() ) {
			info.append(" + [ ");
			info.append(p.informarPedido().informarNumeroPedido());
			info.append(" - ");
			info.append(p.informarCodigoPaquete());
			info.append(" ] ");
			info.append(p.informarDireccion());
			info.append("\n");
		}
		return info.toString();
	}

		

}

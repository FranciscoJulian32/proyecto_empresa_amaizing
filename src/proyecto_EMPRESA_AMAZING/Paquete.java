package proyecto_EMPRESA_AMAZING;

public abstract class Paquete {
	private Integer codPaquete; 
	private Integer volumenPaquete;
	private double precio;
	private boolean fueEntregado;
	private Pedido pedido;
	
	public Paquete(Integer codPaquete, Integer volumenPaquete,  double precio,Pedido pedido){
		
		this.codPaquete=codPaquete;
		this.volumenPaquete = volumenPaquete;	
		this.precio = precio;
		this.pedido=pedido;
		this.fueEntregado=false;
		
		
	}
	public Integer informarCodigoPaquete() {
		return codPaquete;
	}
	public Integer informarVolumen() {
		return volumenPaquete;
		
	}
	public String informarDireccion() {
		return pedido.infomarDireccion();
	}
	public double informarPrecio() {
		return precio;
	}
	public Pedido informarPedido() {
		return pedido;
	}
	public boolean fueEntregado() {
		return fueEntregado;
	}
	public void entregar() {
		fueEntregado=true;
	}
	public abstract boolean sonPaquetesIguales(Object o);
	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append("[").append(pedido.informarNumeroPedido()).append("-").append(informarCodigoPaquete()).
		append("] ").append(pedido.infomarDireccion());
		
		return st.toString();
	}
	
	
	
}

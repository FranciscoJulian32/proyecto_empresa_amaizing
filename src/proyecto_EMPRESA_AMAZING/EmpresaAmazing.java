package proyecto_EMPRESA_AMAZING;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Iterator;

public class EmpresaAmazing {
	
	private String nroCuit;
	private Map<String, Transporte> transportes;	// clave = identificador(patente) y valor = clase transporte
	private Map<Integer,Cliente> listaClientes;	//clave = Dni cliente y valor = clase cliente
	private Map<Integer,Pedido> pedidos;	//pedidos ya finalizados
	private int codPaqueteEspecial;
	private int codPaqueteOrdinario;
	private int codPedidos;
	private double facturacionTotal;
	
	
	public EmpresaAmazing(String string) {	// String string sino me daba error el main 
		this.nroCuit="11-12345678-0";
		this.listaClientes = new HashMap<>();
		this.pedidos = new HashMap<>();
		this.transportes = new HashMap<>();
		this.codPaqueteEspecial=1;
		this.codPaqueteOrdinario=1;
		this.codPedidos=1;
		this.facturacionTotal=0;
	}
	
	public EmpresaAmazing(Integer nroCuit, Map<Integer, Paquete> pedidosClientes, Map<String, Transporte> transportes,
			Map<Integer, Cliente> listaClientes, Map<Integer,Pedido> pedidos) {
		
		this.nroCuit = "11-12345678-0";
		this.transportes = transportes;
		this.listaClientes = listaClientes;
		this.pedidos = pedidos;
		this.codPaqueteEspecial=1;
		this.codPaqueteOrdinario=1;
		this.codPedidos=1;
		this.facturacionTotal=0;
	}

	/**
	 * Registra un nuevo transporte tipo Automovil en el sistema con los siguientes 
	 * datos que corresponden a toda clase de transporte:
	 *  - patente, 
	 *  - volumen maximo de carga
	 *  - valor del viaje (que cobrara la empresa)
	 *  
	 * Ademas por ser Automovil se proporciona el dato:
	 *  - cantidad maxima de paquetes que transporta
	 *  
	 * Si esa patente ya esta en el sistema se debe generar una  excepcion.
	 */
	public void registrarAutomovil(String patente, int volMax, int valorViaje, int maxPaq){
		if(transportes.containsKey(patente)) {
			throw new RuntimeException("El Auto ya esta registrado");
		}
		TransporteComun transporte = new TransporteComun(patente,volMax,valorViaje,maxPaq);
		transportes.put(patente, transporte);
		
	}
	
	
	
	/**
	 * Registra un nuevo transporte tipo Utilitario en el sistema con los  
	 * datos correspondientes a toda clase de transporte y ademas:
	 * 
	 *  - un valor extra que cobra a la empresa si superan los 10 paquetes.
	 * 
	 * Si esa patente ya esta en el sistema se debe generar una  excepcion.
	 */
	public void registrarUtilitario(String patente, int volMax, int valorViaje, int valorExtra) {
		if(transportes.containsKey(patente)) {
			throw new RuntimeException("El Utilitario ya esta registrado");
		}
		TransporteUtilitarios transporte = new TransporteUtilitarios(patente,volMax,valorViaje,valorExtra);
		transportes.put(patente, transporte);
		
	} //mayuscula a los nombres donde corresponda
	
	/**
	 * Registra un nuevo transporte tipo Camion en el sistema con los  
	 * datos correspondiente a todo transporte y ademas:
	 * 
	 *  - un valor adicional que se multiplica por la cantidad de paquetes que le carguen.
	 * 
	 * Si esa patente ya esta en el sistema se debe generar una  excepcion.
	 */
	public void registrarCamion(String patente, int volMax, int valorViaje, int adicXPaq) {
		if(transportes.containsKey(patente)) {
			throw new RuntimeException("El camion ya esta registrado");
		}
		TransporteCamion transporte = new TransporteCamion(patente,volMax,valorViaje,adicXPaq); //String patente, Integer volumenCarga, double valorViaje,Integer maxPaq, double adicXPaq
		transportes.put(patente, transporte);
	}
	/**
	 * Se registra un nuevo pedido en el sistema proporcionando los siguientes datos:
	 * - el nombre del cliente que lo solicita
	 * - su dirección
	 * - su dni
	 * 
	 * El sistema le asigna un numero de pedido unico y crea un carrito de ventas vacio.
	 * Devuelve el numero de pedido asignado.
	 * 
	 */
	public int registrarPedido(String cliente, String direccion, int dni) {
		Cliente nuevoCliente = new Cliente (cliente,direccion,dni);	
		Pedido nuevoPedido = new Pedido(codPedidos,nuevoCliente);//creamos el nuevo pedido, aca tengo dudad sobre los atributos que toma, pq en el contructor de la clase pedido toma mas atributos
		int numeroPedido= codPedidos;
		pedidos.put(numeroPedido, nuevoPedido);
		codPedidos++;
		return numeroPedido; //retornamos el numero de pedido unico que ya lo creamos en otro metodo
	}
	
	
	
	
//	 * Se registra la compra de un producto, el cual se agregara al carrito del pedido dado 
//	 * como un paquete de tipo ordinario. 
//	 * 
//	 * Se ingresan los datos necesario para agregarlo:
//	 *  - pedido al que corresponde agregarlo
//	 *  - volumen del paquete a agregar
//	 *  - precio del producto que contiene el paquete.
//	 *  
//	 *  Ademas por ser un paquete de tipo ordinario:
//	 *  - costo del envio
//	 * 
//	 *  Si ese pedido no esta registrado en el sistema o ya está finalizado
//	 *  se debe generar una  excepcion.
//	 * 
//	 * devuelve el codigo de paquete unico.
//	 * 
//	 */
	public int agregarPaquete(int codPedido, int volumen, int precio, int costoEnvio) {
		if(!pedidos.containsKey(codPedido)){
			throw new RuntimeException("el pedido no se encuentra en el sistema");
		}
		Pedido p=pedidos.get(codPedido);
		if(p.informarEstado()) {	
			throw new RuntimeException("Pedido terminado");
		}
		PaqueteOrdinario paqueteOrd = new PaqueteOrdinario(codPaqueteOrdinario, volumen, precio, costoEnvio,pedidos.get(codPedido)); 
		codPaqueteOrdinario++;
		pedidos.get(codPedido).agregarPaquete(paqueteOrd);  
		return paqueteOrd.informarCodigoPaquete();
	}
	
	/**
	 * Se registra la compra de un producto que se agregara al carrito del pedido dado 
	 * como paquete de tipo especial. 
	 * 
	 * Se ingresan los datos necesario para agregarlo:
	 *  - pedido al que corresponde agregarlo
	 *  - volumen del paquete a agregar
	 *  - precio del producto que contiene el paquete.
	 *  
	 *  Ademas por ser un paquete de tipo especial:
	 *  - porcentaje adicional (que se calcula y suma a su precio)
	 *  - adicional (se suma si el paquete tiene volumen>3000)
	 * 
	 *  Si ese pedido no esta registrado en el sistema o ya está finalizado
	 *  se debe generar una  excepcion.
	 * 
	 * devuelve el codigo de paquete unico.
	 * 
	 */
	public int agregarPaquete(int codPedido, int volumen, int precio, int porcentaje, int adicional) {
		if(!pedidos.containsKey(codPedido)){
			throw new RuntimeException("el pedido no se encuentra en el sistema");
		}
		Pedido p=pedidos.get(codPedido);
		if(p.informarEstado()) {	
			throw new RuntimeException("Pedido terminado");
		}
		PaqueteEspecial paqueteEsp= new PaqueteEspecial(codPaqueteEspecial,volumen,precio,porcentaje,adicional,pedidos.get(codPedido));
		codPaqueteEspecial++;
		pedidos.get(codPedido).agregarPaquete(paqueteEsp);
		return paqueteEsp.informarCodigoPaquete();
	}
	/**
	 * quita un paquete del pedido dado su codigo unico de paquete.
	 * 
	 * Devuelve true si pudo quitar el paquete. 
	 * si no lo encontró o  el pedido ya esta finalizado, devuelve false.
	 * 
	 * Demostrar la complejidad en terminos de O grande en el informe.
	 */
	public boolean quitarPaquete(int codPaquete) {
		Iterator<Pedido> it = pedidos.values().iterator();
		while(it.hasNext()) {
			Pedido p= it.next();
			if (p.informarEstado()&&p.contienePaquete(codPaquete)) {
				p.removerPaquete(codPaquete);
				return true;
			}
			if ((!p.informarEstado())&&p.contienePaquete(codPaquete)) {
				return false;
			}
			if(!p.contienePaquete(codPaquete)) {
				throw new RuntimeException("el paquete no existe");
			}
		}	
		return false;
	}

	


	/**
	 * Se registra la finalizacion de un pedido registrado en la empresa, 
	 * dado su codigo.
	 * Devuelve el total a pagar por el pedido.
	 * 
	 * Si ese codigo no esta en el sistema o ya fue finalizado se debe 
	 * generar una excepcion.
	 *
	 */
	public double cerrarPedido(int codPedido) {
		if(!pedidos.containsKey(codPedido)) { 
			throw new RuntimeException("El codigo no esta registrado en el Sistema");
		}// lanzamos excepcion sino esta registrado el pedido
		Pedido pedido = pedidos.get(codPedido);
		if (!pedido.informarEstado()) {
			pedido.pedidoFueGestionado();
			facturacionTotal+=pedido.calcularValorPedido(); //CALCULA LOS PEDIDOS CERRADOS 
			return pedido.calcularValorPedido();
		}
		throw new RuntimeException("El pedido ya fue cerrado");
	}

	
	/**
	 * Se registra la carga de un transporte registrado en la empresa, dada su patente.
	 * 
	 * Devuelve un String con forma de listado donde cada renglon representa un 
	 * paquete cargado.
	 * Cada renglon debe respetar el siguiente formato: 
	 *      " + [ NroPedido - codPaquete ] direccion"
	 * por ejemplo:
	 *      " + [ 1002 - 101 ] Gutierrez 1147"
	 *      
	 * Si esa patente no esta en el sistema se debe generar una  excepcion. 
	 * Si esta terminado y no hay productos comprados devuelve [].
	 *
	 */
	public String cargarTransporte(String patente) {
		if (!transportes.containsKey(patente))
			throw new IllegalArgumentException("no existe el vehiculo");
		Transporte t = transportes.get(patente);
		Iterator<Pedido> it = pedidos.values().iterator();
		while(it.hasNext()) {
			Pedido p= it.next();
			if (p.informarEstado()) {
				HashSet<Paquete> paquetes = p.obtenerPaquetes();
				for(Paquete paq : paquetes) {
					if (!paq.fueEntregado()) {
						boolean cargado= t.cargar(paq);
						if (cargado) {
							p.aumentarPaquetesCargados();
							paq.entregar();
						}
					}
				}
			}
		}
		return t.listarCarga();
	}
	
	
	
//	/**
//	 * Se registra el costo del viaje de un transporte dado su patente
//	 * Este costo es el que cobra el transporte (a la empresa) por entregar 
//	 * la carga una vez que fue cargado con los paquetes.
//	 * 
//	 * Una vez cargado, aunque no se haya podido completar, si no hay más paquetes
//	 * para agregarle, el transporte reparte los paquetes cargados.
//	 *  
//	 * Se devuelve el valor del viaje segun lo indicado en cada clase de transporte.
//	 * Cada clase de transporte tiene su forma de calcular el costo del viaje.
//	 *  
//	 * Si esa patente no esta en el sistema se debe generar una excepcion.
//	 * Si el transporte no esta cargado genera un excepcion.
//	 * 
//	 * Se debe resolver en O(1)
//	 */
	public double costoEntrega(String patente) {
		if (!transportes.containsKey(patente)) {
			throw new RuntimeException("No existe el vehiculo");
		}
		double carga = 0;
		Transporte t = transportes.get(patente);
		if (t.cantPaquetesCargados()==0)
			throw new RuntimeException("el vehiculo no tiene paquetes cargados");
		return t.calcularCosto();
	}
	
	
	
	/**
	 * Devuelve la suma del precio facturado de todos los pedidos cerrados.
	 * 
	 * Se debe realizar esta operacion en O(1).
	 */
	public double facturacionTotalPedidosCerrados() {
		return facturacionTotal;
	}
	
	/**
	 * Devuelve los pedidos cerrados y que no fueron entregados en su totalidad. 
	 * O sea, que tienen paquetes sin entregar.
	 * 
	 * Devuelve un diccionario cuya clave es el codigo del pedido y el valor es el nombre del 
	 * cliente que lo pidio.
	 * 
	 */
	public Map<Integer,String> pedidosNoEntregados() {
		if(pedidos.size()==0) {
			throw new RuntimeException("Aun no se han cargado pedidos");
		}
		Map<Integer,String> listadoPedidos= new HashMap<>();
		for (Pedido p : pedidos.values()) {
			if (p.tienePaqNoEntregado()&&(p.informarEstado())) {
				String nombre = p.informarNombreDeCliente();
				Integer nroPedido= p.informarNumeroPedido();
				listadoPedidos.put(nroPedido, nombre);

			}
		}
		return listadoPedidos;
	}
	
	/**
	 * Se consideran transportes identicos a 2 transportes cargados con:
	 *   - distinta patente, 
	 *   - mismo tipo y 
	 *   - la misma carga. //verificar en transporte
	 * Se considera misma carga al tener la misma cantidad de paquetes con las 
	 * mismas caracteristicas:
	 *   - mismo volumen, 
	 *   - mismo tipo 
	 *   - mismo precio y
	 *   - mismos atributos según el tipo de Paquete 
	 *   -//verificar en paquete *   
	 *   VER EJEMPLO EN ENUNCIADO
	 */
	public boolean hayTransportesIdenticos() {
		boolean existe = false;
		for (Transporte t1:transportes.values()) {
			for (Transporte t2:transportes.values()) {
					existe=existe||t1.sonIdenticos(t2);
				}
			}
		return existe;
	}

	@Override
	public String toString() {
		return "EmpresaAmazing [nroCuit=" + nroCuit + "]";
	}
	

	
}


	




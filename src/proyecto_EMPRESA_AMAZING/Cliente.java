package proyecto_EMPRESA_AMAZING;

public class Cliente {
	
	private Integer DNI;	
	private String nombreCliente;
	private String direccion;

	
	public Cliente(String nombreCliente, String direccion,Integer DNI) {
		this.nombreCliente = nombreCliente;
		this.direccion = direccion;
		this.DNI=DNI;
	}

	public Integer getDNI() {
		return DNI;
	}

	
	public void setDNI(Integer dNI) {
		DNI = dNI;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	

}

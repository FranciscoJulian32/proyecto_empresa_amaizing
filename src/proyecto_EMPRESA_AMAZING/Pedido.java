package proyecto_EMPRESA_AMAZING;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Pedido {
	private Integer numeroDePedido;
	private Cliente cliente;
	private int paquetesCargados;
	private double valorPedido;
	private boolean gestionado;
	private Map<Integer,PaqueteEspecial>especiales;
	private Map<Integer,PaqueteOrdinario>ordinarios;
	private PaqueteOrdinario paqOrd;
	private PaqueteEspecial paqEsp;

	
	
	public Pedido(Integer numeroDePedido,Cliente cliente) {
		this.paqEsp=paqEsp;
		this.paqOrd=paqOrd;
		this.numeroDePedido = numeroDePedido;
		this.cliente=cliente;
		this.paquetesCargados=0;
		this.valorPedido=0;
		this.especiales=new HashMap<>();
		this.ordinarios=new HashMap<>();
		this.gestionado=false;
		
	}
//CASTEO DE ORDINARIOS
	public HashSet<PaqueteOrdinario> obtenerPaquetesOrdinarios(){
		HashSet<PaqueteOrdinario> ret = new HashSet<PaqueteOrdinario>();
		ret.addAll(ordinarios.values());
		return ret;
	}
//CASTEO DE ESPECIALES
	public HashSet<PaqueteEspecial> obtenerPaquetesEspeciales(){
		HashSet<PaqueteEspecial> ret = new HashSet<PaqueteEspecial>();
		ret.addAll(especiales.values());
		return ret;
	}
//CASTEO DE PAQUETES
	public HashSet<Paquete> obtenerPaquetes(){
		HashSet<Paquete> ret = new HashSet<Paquete>();
		ret.addAll(especiales.values());
		ret.addAll(ordinarios.values());
		return ret;
	}
//-------------------------------------------------------------------------------------------//
	public Integer informarNumeroPedido() {
		return numeroDePedido;
	}
	public String informarNombreDeCliente() {
		return cliente.getNombreCliente();
	}
	
	public String infomarDireccion() {
		return cliente.getDireccion();
	}
	public int informarCargados() {
		return (ordinarios.size() + especiales.size());
	}
//informa si esta cerrado o no 
	public boolean informarEstado() {
		return gestionado;
	}
	public boolean tienePaqNoEntregado() {
		for (Paquete p : ordinarios.values()) {
			if(!p.fueEntregado()) {
				return true;
			}
		}
		for (Paquete p : especiales.values()) {
			if(!p.fueEntregado()) {
				return true;
			}
		}
		return false;
	}
//remover paquetes del pedido
	public boolean removerPaquete(int codPaquete) {
		if (especiales.containsKey(codPaquete)) {
			especiales.remove(codPaquete);
			return true;
		}
		if(ordinarios.containsKey(codPaquete)) {
			ordinarios.remove(codPaquete);
			return true;
		}
		return false;
	}
	public boolean contienePaquete(int codPaquete){
		if (especiales.containsKey(codPaquete)||ordinarios.containsKey(codPaquete)) {
			return true;
		}
		return false;
	}
//cerrar pedido
	public void pedidoFueGestionado() {
		gestionado = true;
	} 
	public void aumentarPaquetesCargados() {
		paquetesCargados++;
	}
//corrobora que todos los paquetes se hayan cargado
	public boolean seCargaronTodos(){
		return informarCargados()==paquetesCargados;
		
	}
//-------------------------------------------------------------------------------------------//
	public double calcularValorPedido() { //calcular el valor del pedido
		double contador = 0;
		for (Paquete p : ordinarios.values()) {
			contador+=p.informarPrecio();
		}
		for (Paquete p : especiales.values()) {
			contador+=p.informarPrecio();
		}
		return contador;
	}
	
//-------------------------------------------------------------------------------------------//

	public void agregarPaquete(Paquete paquete){
		if (paquete instanceof PaqueteEspecial) {
			especiales.put(paquete.informarCodigoPaquete(), (PaqueteEspecial) paquete);
		}else {
		ordinarios.put(paquete.informarCodigoPaquete(),(PaqueteOrdinario)paquete); 
		}
	}

	@Override
	public String toString() {
		return "Pedido [numeroDePedido=" + numeroDePedido + "]";
	}
	
}

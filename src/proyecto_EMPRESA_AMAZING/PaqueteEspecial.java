package proyecto_EMPRESA_AMAZING;

public class PaqueteEspecial extends Paquete {
	
	private Integer porcentajeExtra;
	private Integer sumaAdicional;
	public PaqueteEspecial(Integer codPaquete, Integer volumenPaquete, double precio,
			Integer porcentajeExtra, Integer sumaAdicional,Pedido pedido) {
		super(codPaquete, volumenPaquete, precio,pedido);
		this.porcentajeExtra = porcentajeExtra;
		this.sumaAdicional = sumaAdicional;
	}
	//CODIGO UNICO IDEnTIFICATORIO 
	public Integer obtenerIdentificacion() {
		return super.informarCodigoPaquete();
	}
	//INFORMAR PRECIO
	public double informarPrecio() {
		double precioBase = super.informarPrecio()+(super.informarPrecio()*porcentajeExtra)/100;
		if ( informarVolumenDePaquete()>=3000)
			return precioBase+sumaAdicional;
		if (informarVolumenDePaquete()>=5000)
			return precioBase+(sumaAdicional*2);
		return precioBase;	
	}
	//INFORMAR VOLUMEN DE PAQUETE
	public Integer informarVolumenDePaquete() {
		return super.informarVolumen();
	}
//INFORMA NRO PEDIDO AL QUE PERTENECE
		public boolean informarEstadoPedido() {
			return this.informarPedido().informarEstado();
		}
//INFORMAR PRECIO
		public double precioBase() {
			return super.informarPrecio();
		}
//EQUALS DE PAQUETES
@Override 
	public boolean sonPaquetesIguales(Object o) {
		PaqueteEspecial paqEspecial = (PaqueteEspecial) o;
		
		boolean sonIguales= paqEspecial.getClass().getName().equals(this.getClass().getName())&&
				paqEspecial.informarVolumen()==this.informarVolumen()&&
				paqEspecial.precioBase()==this.precioBase()&&
				paqEspecial.porcentajeExtra==this.porcentajeExtra&&
				paqEspecial.sumaAdicional==this.sumaAdicional;
		return sonIguales;
				
	}
	@Override
	public String toString() {
		return "PaqueteEspecial [porcentajeExtra=" + porcentajeExtra + ", sumaAdicional=" + sumaAdicional
				+ ", informarPrecio()=" + informarPrecio() + "]";
	}
	
	
	

}

package proyecto_EMPRESA_AMAZING;

public class PaqueteOrdinario extends Paquete{

	private double costoEnvio;
	

	public PaqueteOrdinario(Integer codIdentificacion, Integer volumenPaquete, double precio,
			double costoEnvio,Pedido pedido) {
		super(codIdentificacion, volumenPaquete, precio,pedido);
		this.costoEnvio = costoEnvio;
	}
//INFORMAR PRECIO
	public double informarPrecio() {
		return super.informarPrecio()+costoEnvio;	
	}
//INFORMA NRO PEDIDO AL QUE PERTENECE
	public boolean informarEstadoPedido() {
		return this.informarPedido().informarEstado();
	}
//INFORMAR VOLUMEN DE PAQUETE
	public Integer informarVolumenPaquete() {
		return super.informarVolumen();
	}
//CODIGO UNICO IDEnTIFICATORIO 
	public Integer obtenerIdentificacion() {
		return super.informarCodigoPaquete();
	}
	@Override 
	public boolean sonPaquetesIguales(Object o) {
		PaqueteOrdinario paqOrdinario = (PaqueteOrdinario) o;
		
		boolean sonIguales= paqOrdinario.getClass().getName().equals(this.getClass().getName())&&
				paqOrdinario.informarVolumen()==this.informarVolumen()&&
				paqOrdinario.precioBase()==this.precioBase()&&
				paqOrdinario.costoEnvio==this.costoEnvio;
		return sonIguales;
				
	}
//AUX
	public double precioBase() {
		return super.informarPrecio();
	}
//INFORMAR 
	@Override
	public String toString() {
		return super.toString()+"paqueteOrdinario [costoEnvio=" + costoEnvio + "]";
	}	
	}



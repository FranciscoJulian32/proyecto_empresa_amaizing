package proyecto_EMPRESA_AMAZING;

import java.util.*;



public class TransporteCamion extends Transporte {

	private double adicXPaq;
	private Map<Integer,PaqueteEspecial> especiales;
	private Integer maxPaq;

	public TransporteCamion(String patente, Integer volumenCarga, double valorViaje, double adicXPaq) {
		super(patente, volumenCarga, valorViaje,"Camion"); //String patente, int volMax, int valorViaje, int adicXPaq
		this.adicXPaq=adicXPaq;
		this.especiales=new HashMap<>();
		this.maxPaq=1000;
		
	}

//INFORMA COSTO POR VIAJE
@Override 
	public double calcularCosto() {
		return super.getValorViaje()+costoAdicional();
	}
//INFORME EL VOLUMEN TOTAL DE LA CARGA
	public Integer getVolumenCarga() {
		int cont =0;
		for (PaqueteEspecial p: especiales.values()) {
			cont =+ p.informarVolumen();
		}
		return cont;
	}
//CALCULA EL COSTO ADICC
	private double costoAdicional() {
		return especiales.size()*adicXPaq;
	}
//INFORMA EL ESPACIO DISPO DEL VEHICULO 
	private boolean consultarEspacio() {
		return cantPaquetesCargados() <= maxPaq; 
	}
	public int cantPaquetesCargados(){
		return especiales.size();
	}
//CARGAr EL VEHICULO
@Override 
	public boolean cargar(Paquete paq) {
		if (!(paq instanceof PaqueteEspecial)) {
			return false;
		}
		if (paq.informarVolumen()>=2000 && cantPaquetesCargados()<=maxPaq) {
			especiales.put(paq.informarCodigoPaquete(),(PaqueteEspecial) paq);
			return true;
		}
		return false;
		
	}
//INFORMA PATENTE
	public String informarPatente() {
		return super.getPatente();
	}
//INFORMAR PAQUETES 
	private Map<Integer,PaqueteEspecial> informaCarga(){
		return especiales;
	}
//ESTA CARGADO 
	public boolean estaCargado(){
		return cantPaquetesCargados()>0;
	}
//SON IDENTICOS AUTO
	
//SON IDENTICOS
	@Override 
	public boolean sonIdenticos(Object o) {
		if (!(o instanceof TransporteCamion)||o==null) {
			return false;
		}
		TransporteCamion camion = (TransporteCamion) o;
		if (camion.informarPatente().equals(super.getPatente())) {
			return false;
		}
		if (camion.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
			return false;
		}
		if (camion.cantPaquetesCargados()==0||this.cantPaquetesCargados()==0) {
			return false;
		}
		return mismaCarga(camion)&&tieneTodos(camion);
	}
//AUX
	private  boolean mismaCarga(TransporteCamion camion ) {
		if (camion.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
			return false;
		}
		boolean ret = false;
		for (PaqueteEspecial p:especiales.values()) {
			PaqueteEspecial p1 = p;
			for(PaqueteEspecial paq:camion.carga().values()) {
				if (p1.informarCodigoPaquete()!=paq.informarCodigoPaquete())
					ret = ret || p1.sonPaquetesIguales(paq);
				}
			}
		return ret;
	}
//AUX
	public boolean tieneTodos(TransporteCamion camion ) {
		if (camion.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
			return false;
		}

		for (PaqueteEspecial p:especiales.values()) {
			if (!esta(p,carga()))
					return false;	
			}
		return true;
	}
//AUX
	public boolean esta(PaqueteEspecial p,HashMap<Integer,PaqueteEspecial> esp) {
		if (!esp.containsValue(p))
			return false;
		return true;
	}
	
	public HashMap<Integer,PaqueteEspecial> carga(){
		return (HashMap<Integer, PaqueteEspecial>) especiales;
	}
//LISTA LA CARGA
@Override 
	public String listarCarga() {
		StringBuilder info = new StringBuilder();
		for (Paquete p: especiales.values() ) {
			info.append(" + [ ");
			info.append(p.informarPedido().informarNumeroPedido());
			info.append(" - ");
			info.append(p.informarCodigoPaquete());
			info.append(" ] ");
			info.append(p.informarDireccion());
			info.append("\n");
		}
		return info.toString();
	}


}

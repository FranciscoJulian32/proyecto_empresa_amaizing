package proyecto_EMPRESA_AMAZING;

import java.util.HashMap;
import java.util.Map;

public class TransporteComun extends Transporte{
	private Map<Integer,PaqueteOrdinario> ordinarios;
	private Integer maxPaq;
	private Integer cantidadCargados;
 
	
	public TransporteComun(String patente, Integer volumenCarga, double valorViaje,int maxPaq) {
		super (patente,volumenCarga,valorViaje,"Comun");
		this.ordinarios=new HashMap<>();
		this.maxPaq=20;
		this.cantidadCargados=0;
	}
//INFORMA COSTO POR VIAJE
	public double calcularCosto() {
		return super.getValorViaje();
	}
//INFORME EL VOLUMEN TOTAL DE LA CARGA
	public Integer getVolumenCarga() {
		int cont =0;
		for (PaqueteOrdinario p : ordinarios.values()) {
			cont =+ p.informarVolumen();
		}
		return cont;
	}
//CARGAR VEHICULO 
@Override
	public boolean cargar(Paquete paq) {
		if (!(paq instanceof PaqueteOrdinario)) {
			return false;
		}
		if (paq.informarVolumen() <=2000 && cantidadCargados<=maxPaq) {
			ordinarios.put(paq.informarCodigoPaquete(),(PaqueteOrdinario) paq);
			cantidadCargados++;
			return true;
		}
		return false;
	}
//INFORMA LA PATENTE
	public String informarPatente() {
		return super.getPatente();
	}
//CONSULTA LA DISPONIBILIDAD DE ESPACIO 
	private boolean consultarEspacioAuto() {
		return cantPaquetesCargados() <= maxPaq; 
	}
	public int cantPaquetesCargados() {
		return ordinarios.size();
	}
//ESTA CARGADO 
	public boolean estaCargado(){
		return cantPaquetesCargados()>0;
	}
//INFORMA LA CARGA 
	public Map<Integer,PaqueteOrdinario> informarCarga(){
		return ordinarios;
	}
	//SON IDENTICOS
		@Override 
		public boolean sonIdenticos(Object o) {
			//SON DEL MISMO TIPO
			if (!(o instanceof TransporteComun)||o==null) {
				return false;
			}
			TransporteComun auto = (TransporteComun) o;
			//SON DIFERENTE PATENTE
			if (auto.informarPatente().equals(super.getPatente())) {
				return false;
			}
			if (auto.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
				return false;
			}
			if (auto.cantPaquetesCargados()==0||this.cantPaquetesCargados()==0) {
				return false;
			}
			return mismaCarga(auto)&&tieneTodos(auto);
		}
		private  boolean mismaCarga(TransporteComun auto ) {
			if (auto.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
				return false;
			}
			boolean ret = false;
			for (PaqueteOrdinario p:ordinarios.values()) {
				PaqueteOrdinario p1 = p;
				for(PaqueteOrdinario paq:auto.informarCarga().values()) {
					if (p1.informarCodigoPaquete()!=paq.informarCodigoPaquete()) {
						ret = ret || p1.sonPaquetesIguales(paq);
					}
				}
			}
			return ret;
		}
		public boolean tieneTodos(TransporteComun auto ) {
			if (auto.cantPaquetesCargados()!= this.cantPaquetesCargados()) {
				return false;
			}

			for (PaqueteOrdinario p:ordinarios.values()) {
				if (!esta(p,informarCarga()))
						return false;	
				}
			return true;
		}
		public boolean esta(PaqueteOrdinario p,Map<Integer, PaqueteOrdinario> ord) {
			if (!ord.containsValue(p))
				return false;
			return true;
		}
		
//LISTA LA CARGA
	@Override 

	public String listarCarga() {
		StringBuilder info = new StringBuilder();
		for (Paquete p: ordinarios.values() ) {
			info.append(" + [ ");
			info.append(p.informarPedido().informarNumeroPedido());
			info.append(" - ");
			info.append(p.informarCodigoPaquete());
			info.append(" ] ");
			info.append(p.informarDireccion());
			info.append("\n");
		}
		return info.toString();
	}
}
